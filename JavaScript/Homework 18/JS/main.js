"use strict"

const newObj = {
    FirstName: "Sasha",
    lastName: {
        fullName: "Blablabla",
        arr: [
            1,
            2,
        ],
    },
    age: 24,
};

const deepClone = (dest, obj) => {
    for (let key in obj) {
        if(typeof obj[key] === "object") {
            dest[key] = deepClone({}, obj[key]);
            continue;
        }
        dest[key] = obj[key];
    }
    return dest;
}

const check = {};

deepClone(check, newObj);
check.lastName.fullName = 123;
newObj.lastName.arr[0] = 50;
console.log("Clone", check);
console.log("Natiive obj", newObj);
