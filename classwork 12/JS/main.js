'use strict'


/**
 * При натисканні на кнопку Validate відображати
 * слово VALID зеленим кольром, якщо значення проходить валідацію
 * слово INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів (indexof " ", trim() )
 *
 * Элемент з cловом створити за допомогою js
 */


// const input = document.querySelector("#input");
// const inputBtn = document.querySelector("#validate-btn");


// const validateNum = (str) => {
//     if(str && str.trim().length >= 5 && !str.trim().includes(" ")){
//         return `<p class="valid-elem">${str}</p>`;
//     }
//     return `<p class="invalid-elem">${str}</p>`;
// }

// inputBtn.addEventListener("click", () => {
//     document.querySelector("p")?.remove();
//     document.body.insertAdjacentHTML("beforeend", validateNum(input.value));
// });


const beacon = [
    'https://baconmockup.com/600/300/',
    'https://baconmockup.com/600/301/',
    'https://baconmockup.com/600/302/',
    'https://baconmockup.com/600/303/',
    'https://baconmockup.com/600/299/',
];

const bear = [
    'https://placebear.com/600/303',
    'https://placebear.com/600/302',
    'https://placebear.com/600/301',
    'https://placebear.com/600/300',
    'https://placebear.com/600/299',
    'https://placebear.com/600/298',
    'https://placebear.com/600/297',
    'https://placebear.com/600/296',
    'https://placebear.com/600/295',
];


const btnPrev = document.querySelector(".btn-prev");
const btnNext = document.querySelector(".btn-next");


const categories = {
    beacon,
    bear,
}

let currentImg = 0; 
let currentCategory = `beacon`;


const imgContainer = document.body.querySelector(".image-holder");



btnPrev.addEventListener("click", () => {
    if(currentImg > 0) {
        currentImg -= 1;
        imgContainer.style.backgroundImage = `url(${categories[currentCategory][currentImg]})`;
    }
})

btnNext.addEventListener("click", () => {
    if(currentImg < beacon.length - 1) {
        currentImg += 1;
        imgContainer.style.backgroundImage = `url(${categories[currentCategory][currentImg]})`;
    }
})

const btnCollection = document.querySelectorAll(".category-container button");


btnCollection.forEach(btn => {
    btn.addEventListener("click", (e) => {
        currentCategory = e.target.dataset.category;
        currentImg = 0;
        imgContainer.style.backgroundImage = `url(${categories[currentCategory][currentImg]})`;
    })
});