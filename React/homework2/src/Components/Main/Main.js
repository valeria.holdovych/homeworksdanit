import React from "react";
import Card from "../Card/Card";
import styles from "./Main.module.scss";

class Main extends React.Component {
  render() {
    const { goods, rateClickHandler } = this.props;
    const { goodsWrapper } = styles;
    return (
      <main>
        <h2> E-Shop</h2>
        <div className={goodsWrapper}>
          {goods ? (
            goods.map((el) => (
              <Card
                rateClickHandler={rateClickHandler}
                item={el}
                key={el.EAN}
                id={el.EAN}
              />
            ))
          ) : (
            <h2>Loading</h2>
          )}
        </div>
      </main>
    );
  }
}

export default Main;
