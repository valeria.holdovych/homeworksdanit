import React from "react";
import styles from "./Card.module.scss";
import classNames from "classnames";
import PropTypes from "prop-types";

class Card extends React.PureComponent {
  render() {
    const { item, rateClickHandler, id } = this.props;
    const { wrapper, checked } = styles;
    return (
      <div className={wrapper}>
        <img src={item.image} alt="card_goods"></img>
        <h2>{item.title}</h2>
        <div
          onClick={(e) =>
            e.currentTarget !== e.target ? rateClickHandler(e, id) : null
          }
        >
          {
            <>
              <span className={classNames("fa fa-star", checked)}></span>
              <span
                className={
                  item.rate >= 1
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 2
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 3
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
              <span
                className={
                  item.rate >= 4
                    ? classNames("fa fa-star", checked)
                    : classNames("fa fa-star")
                }
              ></span>
            </>
          }
        </div>
        <p>{item.EAN}</p>
        <p>Description</p>
        <p>{item.color}</p>
        <div>
          <p>{item.price}</p>
          <button>Add to cart</button>
        </div>
      </div>
    );
  }
}

Headers.PropTypes = {
  item: PropTypes.object,
  rateClickHandler: PropTypes.func,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Headers.defaultProps = {
  item: [],
  rateClickHandler: () => {},
  id: "",
};

export default Card;
