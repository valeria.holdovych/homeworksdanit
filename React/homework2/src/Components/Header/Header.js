import React from "react";
import styles from "./Header.module.scss";
import facebook from "./img/facebook.svg";
import gmail from "./img//gmail.svg";
import hangsout from "./img/hangsout.svg";
import twitter from "./img/twitter.svg";
import instagram from "./img/instagram.svg";

class Header extends React.PureComponent {
  render() {
    const { body, socialIcons, container, line, loginCartWrapper } = styles;
    return (
      <header>
        <div className={line}></div>
        <section className={body}>
          <div className={container}>
            <div className={socialIcons}>
              <a href="#">
                <img src={facebook} alt="facebook__icon"></img>
              </a>
              <a href="#">
                <img src={gmail} alt="facebook__icon"></img>
              </a>

              <a href="#">
                <img src={hangsout} alt="facebook__icon"></img>
              </a>

              <a href="#">
                <img src={twitter} alt="facebook__icon"></img>
              </a>

              <a href="#">
                <img src={instagram} alt="facebook__icon"></img>
              </a>
            </div>
          </div>
          <div className={loginCartWrapper}>
            <button>login / Register</button>
            <div>
              <img src="./images/cart/shopping-cart.png"></img>
              {/* <div style={{ display: "block" }}>
                <img src="facebook__icon" alt="facebook-icon"></img>
              </div> */}
            </div>
          </div>
        </section>
      </header>
    );
  }
}

export default Header;
