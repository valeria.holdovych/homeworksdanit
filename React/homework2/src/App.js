import React from "react";
import Header from "./Components/Header/Header";
import Main from "./Components/Main/Main";

class App extends React.Component {
  state = {
    isLoaded: false,
    goods: [],
  };
  handleRateClick = (e, id) => {
    const clickedEl = e.target.closest("span");
    let counter = 0;
    if (clickedEl.classList.contains("Card_checked__PxZIY")) {
      const checkPrevStars = (prevElementNode) => {
        let prevElement = prevElementNode.nextSibling;
        if (prevElement) {
          prevElement.classList.remove("Card_checked__PxZIY");
          checkPrevStars(prevElement);
          counter -= 1;
        } else {
          return;
        }
      };
      checkPrevStars(clickedEl);
    } else {
      clickedEl.classList.add("Card_checked__PxZIY");
      const checkPrevStars = (prevElementNode) => {
        let prevElement = prevElementNode.previousSibling;
        if (prevElement) {
          prevElement.classList.add("Card_checked__PxZIY");
          checkPrevStars(prevElement);
          counter += 1;
        } else {
          return;
        }
      };
      checkPrevStars(clickedEl);
    }
    const findElementIndex = this.state.goods.findIndex(
      ({ EAN }) => EAN === id
    );
    const copiedGoodsArr = this.state.goods;
    copiedGoodsArr[findElementIndex].rate = counter;
    localStorage.setItem("goods", JSON.stringify(copiedGoodsArr));
    this.setState({ goods: [...copiedGoodsArr] });
  };
  async componentDidMount() {
    if (localStorage.goods) {
      const items = JSON.parse(localStorage.getItem("goods"));
      this.setState({ goods: [...items], isLoaded: true });
    } else {
      const goodsArray = await fetch("./items.json").then((res) => res.json());
      this.setState({ goods: [...goodsArray], isLoaded: true });
    }
  }
  render() {
    return (
      <>
        <Header />
        {this.state.isLoaded ? (
          <Main
            rateClickHandler={this.handleRateClick}
            goods={this.state.goods}
          />
        ) : (
          <Main />
        )}
      </>
    );
  }
}

export default App;

// const firstModalText =
//   "Once you delete this file, it won’t be possible to undo this action. \n Are you sure you want to delete it?";
// const secondModalText = "You will be redirected to another page now";

// const { container } = styles;
// const modalStatus = this.state.openModal;
// const clickedBtn = this.state.clickedBtn;
// return (
//   <div className={container}>
//     <div>
//       <Button
//         backgroundColor="blue"
//         handleClick={this.handleOpenModalClick}
//         text="Open first modal"
//       />
//       <Button
//         backgroundColor="yellow"
//         handleClick={this.handleOpenModalClick}
//         text="Open second modal"
//       />
//     </div>
//     {!modalStatus && clickedBtn === "1" && (
//       <Modal
//         handleClick={this.handleOpenModalClick}
//         header="Do you really want to delete this file? "
//         text={firstModalText}
//         needCrossBtn={true}
//         actions={
//           <>
//             <button onClick={this.handleOpenModalClick}>Ok</button>
//             <button onClick={this.handleOpenModalClick}>Cancel</button>
//           </>
//         }
//       />
//     )}
//     {!modalStatus && clickedBtn === "2" && (
//       <Modal
//         handleClick={this.handleOpenModalClick}
//         header="Really want to open second Modal? "
//         text={secondModalText}
//         needCrossBtn={false}
//         actions={
//           <>
//             <button onClick={this.handleOpenModalClick}>Confirm</button>
//             <button onClick={this.handleOpenModalClick}>Go back</button>
//           </>
//         }
//       />
//     )}
//   </div>
